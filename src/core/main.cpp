#include <stdio.h> 
#include <stdlib.h>
#include <string.h>

#define LS(a) (a), (sizeof(a)-1)

class linknodes
{
private:
class linknodes * next;

public:
linknodes();
linknodes * setnext(linknodes * nextb)
{return next = nextb;}

linknodes * getnext()
{return next;}
};

linknodes::linknodes()
{
    next = NULL;
}

class pageblock : private linknodes{
    friend class RetPage;
private:

	size_t alloc_size;
	size_t len;
	void * lbuf;

public:
	pageblock(size_t size);
	~pageblock();


    int copytobuff(const void * buf, size_t size);
	


};

pageblock::pageblock(size_t size)
{
	alloc_size = size;
	len = 0;
	lbuf = malloc(alloc_size);
}


pageblock::~pageblock()
{
	free(lbuf);

}

int pageblock::copytobuff(const void * buf, size_t size)
{

	if(!buf || size==0)
		return 0;

	if(len+size>alloc_size)
		lbuf = realloc(lbuf,len+size);

	memcpy((char*)lbuf+len,buf,size);
    len+=size;
	return size;
}

class RetPage {
private:
	unsigned short returncode;


	pageblock * firstblock, * lastblock;
	size_t pagelen;


public:
	RetPage(void);
	~RetPage(void);
	void addpageblock(void * block, size_t size);
	void addsmallpageblcok(void * block, size_t size);

    int flash(void);
};


RetPage::RetPage()
{
	firstblock = NULL;
	lastblock = NULL;
    pagelen = 0;

    returncode = 200;


}

RetPage::~RetPage()
{
	delete firstblock;
	/*pageblock * nextb;
	while(firstblock){
		nextb = firstblock->next;
		delete firstblock;
		firstblock = nextb;
	};*/


}


void RetPage::addpageblock(void * block, size_t size)
{
    pageblock * newblock = new pageblock(size);
    if(lastblock) lastblock->setnext(newblock);

    lastblock = newblock;
    if(!firstblock) firstblock = newblock;

    newblock->copytobuff(block, size);
    pagelen+=size;

}

int RetPage::flash()
{
    pageblock * headerblock = new pageblock(512);
    char numbuf[20];




headerblock->copytobuff(LS("Content-Length: "));

sprintf(numbuf, "%d", pagelen);

headerblock->copytobuff(numbuf, strlen(numbuf));

headerblock->copytobuff(LS("\r\nContent-Type: text/html\r\n"));

headerblock->copytobuff(LS("\r\n"));


   fwrite(headerblock->lbuf,1,headerblock->len,stdout);
delete headerblock;
    pageblock * nextb;
    while(firstblock){
        fwrite(firstblock->lbuf,1,firstblock->len,stdout);
        nextb = (pageblock*)firstblock->getnext();
        delete firstblock;
        firstblock = nextb;
    };


    //printf("Teszt\n");
    return 0;
}


int main()
{
    RetPage page;



char test[] = "This is out testing";
page.addpageblock(test,strlen(test));
page.addpageblock(test,strlen(test));
page.flash();



    return 0;
}

